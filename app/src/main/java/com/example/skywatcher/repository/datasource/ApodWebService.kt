package com.example.skywatcher.repository.datasource

import android.util.Log
import com.example.skywatcher.BuildConfig
import com.example.skywatcher.util.conversion.CustomDateAdapter
import com.example.skywatcher.util.conversion.MoshiStringConverterFactory
import com.squareup.moshi.Moshi
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

object ApodWebService {
    private var apodAPi: ApodApi? = null

    fun getApi(): ApodApi {
        if (apodAPi == null) {
            val client = OkHttpClient.Builder().apply {
                val inteceptor = HttpLoggingInterceptor {
                    Log.e("ApodRepository", it)
                }
                inteceptor.level = HttpLoggingInterceptor.Level.BODY
                addInterceptor(inteceptor)
            }.build()
            val moshi = Moshi.Builder().add(CustomDateAdapter()).build()

            val api = Retrofit.Builder()
                .client(client)
                .baseUrl(BuildConfig.APOD_API)
                .addConverterFactory(MoshiConverterFactory.create(moshi))
                .addConverterFactory(MoshiStringConverterFactory(moshi))
                .build()
            apodAPi = api.create(ApodApi::class.java)
        }
        return apodAPi as ApodApi
    }
}
