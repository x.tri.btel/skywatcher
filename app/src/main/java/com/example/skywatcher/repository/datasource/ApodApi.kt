package com.example.skywatcher.repository.datasource

import com.example.skywatcher.BuildConfig
import com.example.skywatcher.model.Apod
import retrofit2.http.GET
import retrofit2.http.Query
import java.util.*

interface ApodApi {
    @GET("apod")
    suspend fun getImageOfTheDay(
        @Query("api_key") apiKey: String = BuildConfig.API_KEY,
        @Query("date") date: Date
    ): Apod
}


