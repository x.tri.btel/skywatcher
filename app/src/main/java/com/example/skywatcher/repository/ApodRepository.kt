package com.example.skywatcher.repository

import com.example.skywatcher.model.Apod
import com.example.skywatcher.repository.datasource.ApodWebService
import java.util.*

object ApodRepository {
    suspend fun getPictureForDate(date: Date): Apod {
        return ApodWebService.getApi().getImageOfTheDay(date = date)
    }
}
