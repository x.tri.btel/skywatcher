package com.example.skywatcher.util.conversion

import com.example.skywatcher.util.dateFormat
import com.squareup.moshi.*
import java.util.*

class CustomDateAdapter : JsonAdapter<Date>() {

    @FromJson
    override fun fromJson(reader: JsonReader): Date? {
        val dateAsString = reader.nextString()
        return try {
            dateFormat.parse(dateAsString)
        } catch (e: Exception) {
            null
        }
    }

    @ToJson
    override fun toJson(writer: JsonWriter, value: Date?) {
        writer.value(value?.let { dateFormat.format(it) } ?: "")
    }

}
