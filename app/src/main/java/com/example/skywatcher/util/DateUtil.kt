package com.example.skywatcher.util

import java.text.SimpleDateFormat

val dateFormat = SimpleDateFormat("yyyy-MM-dd")
