package com.example.skywatcher.util.conversion

import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import retrofit2.Converter
import retrofit2.Retrofit
import java.lang.reflect.Type

class MoshiStringConverterFactory(val moshi: Moshi) : Converter.Factory() {
    override fun stringConverter(
        type: Type,
        annotations: Array<out Annotation>,
        retrofit: Retrofit
    ): Converter<*, String> {
        val adapter: JsonAdapter<Any> = moshi.adapter(type)
        return StringConverter<Any>(adapter)
    }

    inner class StringConverter<T>(private val typeAdapter: JsonAdapter<Any>) :
        Converter<T, String> {
        override fun convert(value: T): String? {
            val jsonValue = typeAdapter.toJson(value)
            return if (jsonValue.startsWith("\"") && jsonValue.endsWith("\"")) {
                jsonValue.substring(1, jsonValue.length - 1)
            } else {
                jsonValue
            }

        }
    }
}
