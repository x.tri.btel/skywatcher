package com.example.skywatcher.view

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.skywatcher.model.Apod
import com.example.skywatcher.repository.ApodRepository
import com.example.skywatcher.util.dateFormat
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import java.util.*

data class DateConstraint(val mindDate: Date, val maxDate: Date)

class MainViewModel : ViewModel() {
    private val _dataToLoad: MutableLiveData<Apod> = MutableLiveData()
    private var getPictureJob: Job? = null

    val dataToLoad: LiveData<Apod> = _dataToLoad
    var currentlySelectedDate: Date? = null

    fun selectDate(date: Date) {
        currentlySelectedDate = date
        getPictureJob?.cancel()
        getPictureJob = viewModelScope.launch {
            _dataToLoad.postValue(ApodRepository.getPictureForDate(date))
        }
    }

    fun getDateConstraint(): DateConstraint = DateConstraint(
        dateFormat.parse("1995-06-16"),
        Date()
    )
}
