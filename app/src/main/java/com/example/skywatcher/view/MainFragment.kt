package com.example.skywatcher.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.example.skywatcher.databinding.MainFragmentBinding
import com.example.skywatcher.util.dateFormat
import com.squareup.picasso.Picasso

class MainFragment : Fragment() {

    companion object {
        fun newInstance() = MainFragment()
    }

    private lateinit var binding: MainFragmentBinding
    private val viewModel: MainViewModel by lazy {
        ViewModelProvider(this).get(
            MainViewModel::class.java
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = MainFragmentBinding.inflate(layoutInflater)
        setupCalendar()
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupImageView()
    }

    fun setupCalendar() {
        val dateConstraint = viewModel.getDateConstraint()
        binding.calendar.minDate = dateConstraint.mindDate.time
        binding.calendar.maxDate = dateConstraint.maxDate.time
        binding.calendar.setOnDateChangeListener { calendarView, year, month, dayOfMonth ->
            viewModel.selectDate(dateFormat.parse("$year-${month + 1}-$dayOfMonth"))
        }
        viewModel.currentlySelectedDate?.let {
            binding.calendar.date = it.time
        }
    }

    fun setupImageView() {
        viewModel.dataToLoad.observe(viewLifecycleOwner) {
            binding.image.loadImage(it.url)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
    }

    private fun ImageView.loadImage(url: String): Unit {
        Picasso.get()
            .load(url)
            .fit()
            .into(this)
    }
}
