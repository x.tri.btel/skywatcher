package com.example.skywatcher.model

import com.squareup.moshi.*
import java.util.*

data class Apod(
    @field:Json(name = "date") val date: Date,
    @field:Json(name = "explanation") val explanation: String,
    @field:Json(name = "title") val title: String,
    @field:Json(name = "url") val url: String,
)

