package com.example.skywatcher.view

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.example.skywatcher.model.Apod
import com.example.skywatcher.repository.ApodRepository
import com.example.skywatcher.util.dateFormat
import io.mockk.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.assertj.core.api.Assertions.assertThat
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import java.util.*


class MainViewModelTest {
    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()
    val dispatcher = TestCoroutineDispatcher()

    private lateinit var apodObserver: Observer<Apod>
    private lateinit var underTest: MainViewModel

    @Before
    fun setup() {
        underTest = MainViewModel()
        apodObserver = spyk(Observer<Apod> { })
        mockkObject(ApodRepository)
        Dispatchers.setMain(dispatcher)
    }

    @After
    fun cleanUp() {
        unmockkAll()
        Dispatchers.resetMain()
    }

    @Test
    fun `it should properly select date`() {
        val date = Date()
        val apod = Apod(date, "explain", "title", "url")
        coEvery { ApodRepository.getPictureForDate(any()) } returns apod
        underTest.dataToLoad.observeForever(apodObserver)

        underTest.selectDate(date)

        verify(exactly = 1) { apodObserver.onChanged(apod) }
        assertThat(underTest.currentlySelectedDate).isEqualTo(date)
    }

    @Test
    fun `it should return correct constraints`() {
        val expected = DateConstraint(dateFormat.parse("1995-06-16"), Date())

        val result = underTest.getDateConstraint()

        assertThat(expected).isEqualTo(result)
    }
}
