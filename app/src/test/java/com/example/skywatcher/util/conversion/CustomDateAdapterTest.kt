package com.example.skywatcher.util.conversion

import com.example.skywatcher.util.dateFormat
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test

class CustomDateAdapterTest {
    val underTest: CustomDateAdapter = CustomDateAdapter()

    @Test
    fun `should serilize`() {
        val dateAsString = "2020-10-10"
        val expected = "\"2020-10-10\""
        val date = dateFormat.parse(dateAsString)

        val result = underTest.toJson(date)

        assertThat(result).isEqualTo(expected)
    }

}

